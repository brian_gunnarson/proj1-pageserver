"""
  A trivial web server in Python.

  Based largely on https://docs.python.org/3.4/howto/sockets.html
  This trivial implementation is not robust:  We have omitted decent
  error handling and many other things to keep the illustration as simple
  as possible.

  FIXME:
  Currently this program always serves an ascii graphic of a cat.
  Change it to serve files if they end with .html or .css, and are
  located in ./pages  (where '.' is the directory from which this
  program is run).
"""

import config    # Configure from .ini files and command line
import logging   # Better than print statements
import os        # For finding files
logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.INFO)
log = logging.getLogger(__name__)
# Logging level may be overridden by configuration 

import socket    # Basic TCP/IP communication on the internet
import _thread   # Response computation runs concurrently with main program


def listen(portnum):
    """
    Create and listen to a server socket.
    Args:
       portnum: Integer in range 1024-65535; temporary use ports
           should be in range 49152-65535.
    Returns:
       A server socket, unless connection fails (e.g., because
       the port is already in use).
    """
    # Internet, streaming socket
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Bind to port and make accessible from anywhere that has our IP address
    serversocket.bind(('', portnum))
    serversocket.listen(1)    # A real server would have multiple listeners
    return serversocket


def serve(sock, func):
    """
    Respond to connections on sock.
    Args:
       sock:  A server socket, already listening on some port.
       func:  a function that takes a client socket and does something with it
    Returns: nothing
    Effects:
        For each connection, func is called on a client socket connected
        to the connected client, running concurrently in its own thread.
    """
    while True:
        log.info("Attempting to accept a connection on {}".format(sock))
        (clientsocket, address) = sock.accept()
        _thread.start_new_thread(func, (clientsocket,))


##
# Starter version only serves cat pictures. In fact, only a
# particular cat picture.  This one.
##
CAT = """
     ^ ^
   =(   )=
"""

# HTTP response codes, as the strings we will actually send.
# See:  https://en.wikipedia.org/wiki/List_of_HTTP_status_codes
# or    http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
##
STATUS_OK = "HTTP/1.0 200 OK\n\n"
STATUS_FORBIDDEN = "HTTP/1.0 403 Forbidden\n\n"
STATUS_NOT_FOUND = "HTTP/1.0 404 Not Found\n\n"
STATUS_NOT_IMPLEMENTED = "HTTP/1.0 401 Not Implemented\n\n"

# Get the current working directory and store it in PATH
PATH = os.getcwd()

# The skeleton for this code comes from https://codezup.com/search-find-file-exists-directory-python/
# This article was written December 9, 2019 and published by codezup
# I found this code on October 7, 2020
# I modified the body including adding log statements, updating the path, and opening/reading the specified file
def find_file(filename, sock):
    # Loop through directories in a specified PATH
    for root, dirs, files in os.walk(PATH):
        log.info('Looking in: {}'.format(root))
        # Loop through everything in the directories
        for Files in files:
            try:
                found = Files.find(filename)
                if found != -1:
                    log.info("\n{} found".format(filename))
                    log.info("\nRoot: {}".format(root))

                    # Update the path to include the file name
                    new_path = root + '/' + filename
                    log.info("\nNew Path: {}".format(new_path))

                    # Open and read the contents of the file by using the path we just put together
                    with open(new_path, 'r') as file:
                        contents = file.read()
                    return contents
            except:
                log.info("Can't find file: {}".format(filename))
                return None


def respond(sock):
    """
    This server responds only to GET requests (not PUT, POST, or UPDATE).
    Any valid GET request is answered with an ascii graphic of a cat.
    """
    sent = 0
    request = sock.recv(1024)  # We accept only short requests
    request = str(request, encoding='utf-8', errors='strict')
    log.info("--- Received request ----")
    log.info("Request was {}\n***\n".format(request))

    parts = request.split()

    # Handle forbidden requests
    if (".." in parts[1]) or ("~" in parts[1]) or ("//" in parts[1]):
        log.info("Forbidden request: {}".format(request))
        transmit(STATUS_FORBIDDEN, sock)
        transmit("\nERROR 403 FORBIDDEN", sock)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        return
    elif not (parts[1].endswith(".html") or parts[1].endswith(".css") or parts[1].endswith("/")):
        log.info("Forbidden request: {}".format(request))
        transmit(STATUS_FORBIDDEN, sock)
        transmit("\nERROR 403 FORBIDDEN", sock)
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
        return

    # Parse the request to obtain the desired path
    end_of_path = parts[1].rsplit('/', 1)[-1]
    log.info("Parts: {}".format(parts))

    # Retrieve the contents of the designated path file
    if end_of_path != '':
        contents = find_file(end_of_path, sock)
        log.info("Contents: {}".format(contents))

    # If a path was given but can't be found display a 404 error
    if end_of_path != '' and contents == None:
        transmit(STATUS_NOT_FOUND, sock)
        transmit("\nERROR 404 NOT FOUND", sock)

    # If there is no specified path, display the cat image
    elif len(parts) > 1 and parts[0] == "GET" and end_of_path == '':
        transmit(STATUS_OK, sock)
        transmit(CAT, sock)

    # If there is a specified path, display the contents of the designated file
    elif len(parts) > 1 and parts[0] == "GET" and end_of_path != '':
        transmit(STATUS_OK, sock)
        transmit(contents, sock)

    # Otherwise, display a 401 error
    else:
        log.info("Unhandled request: {}".format(request))
        transmit(STATUS_NOT_IMPLEMENTED, sock)
        transmit("\nI don't handle this request: {}\n".format(request), sock)

    sock.shutdown(socket.SHUT_RDWR)
    sock.close()
    return


def transmit(msg, sock):
    """It might take several sends to get the whole message out"""
    sent = 0
    while sent < len(msg):
        buff = bytes(msg[sent:], encoding="utf-8")
        sent += sock.send(buff)

###
#
# Run from command line
#
###


def get_options():
    """
    Options from command line or configuration file.
    Returns namespace object with option value for port
    """
    # Defaults from configuration files;
    #   on conflict, the last value read has precedence
    options = config.configuration()
    # We want: PORT, DOCROOT, possibly LOGGING

    if options.PORT <= 1000:
        log.warning(("Port {} selected. " +
                         " Ports 0..1000 are reserved \n" +
                         "by the operating system").format(options.port))

    return options


def main():
    options = get_options()
    port = options.PORT
    if options.DEBUG:
        log.setLevel(logging.DEBUG)
    sock = listen(port)
    log.info("Listening on port {}".format(port))
    log.info("Socket is {}".format(sock))
    serve(sock, respond)


if __name__ == "__main__":
    main()
